function exibeNome(){
    var elNome = document.getElementById('txtNome');
    var strNome = elNome.value;
    var elDisplay = document.getElementById('display');
    elDisplay.innerHTML = strNome;
}

function novoContato(){
    var elListaContatos = document.getElementById("listaContatos");
    var divContainer = document.createElement("div");
    var inputContato = document.createElement("input");
    inputContato.setAttribute('class','contatos');
    inputContato.setAttribute('type','number');
    var labelContato = document.createElement("label");
    //<input type="button">
    var btnExcluir = document.createElement("input");
    btnExcluir.setAttribute("type","button");
    btnExcluir.setAttribute("value","-");
    btnExcluir.onclick = function excluir(){
        elListaContatos.removeChild(divContainer);
    };
    labelContato.innerText = "Contato: ";
    elListaContatos.appendChild(divContainer);
    divContainer.appendChild(labelContato);
    divContainer.appendChild(inputContato);
    divContainer.appendChild(btnExcluir);
}

function salvar(){
    var divResultados = document.getElementById("resultados");
    var elNome = document.getElementById('txtNome');
    var strNome = elNome.value;
    if(strNome != ''){
        var labelNome = document.createElement("label");
        var divContainer = document.createElement("div");
        var contatos = document.getElementsByClassName("contatos");
        if(divResultados.hasChildNodes){
            divResultados.removeChild(divResultados.firstChild);
        }
        labelNome.innerText = strNome;
        divContainer.appendChild(labelNome);
        for(var i=0; i < contatos.length; i++){
            var divContato = document.createElement('div');
            var labelContato = document.createElement('label');
            labelContato.innerText = "Contato";
            labelContato.style.marginRight = 5;
            var labelContatoNumero = document.createElement('label');
            labelContatoNumero.innerText = contatos[i].value;
            divContato.appendChild(labelContato);
            divContato.appendChild(labelContatoNumero);
            divContainer.appendChild(divContato);
        }
        divResultados.appendChild(divContainer);
    }

}